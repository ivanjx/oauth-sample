using System;
using System.Net.Http;

namespace oauth_sample.Client.Services;

public class HttpClientFactory : IHttpClientFactory
{
    Uri m_host;

    public HttpClientFactory(string host)
    {
        m_host = new Uri(host);
    }
    
    public HttpClient CreateClient(string name)
    {
        return new HttpClient()
        {
            BaseAddress = m_host
        };
    }
}
