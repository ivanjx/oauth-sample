using System;

namespace oauth_sample.Client.Services;

public class AppState
{
    public string? DisplayName
    {
        get;
        set;
    }
    
    public string? Email
    {
        get;
        set;
    }

    public string? JobTitle
    {
        get;
        set;
    }
}
