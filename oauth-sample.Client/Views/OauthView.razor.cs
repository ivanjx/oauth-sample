using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Components;
using oauth_sample.Client.Services;

namespace oauth_sample.Client.Views;

public partial class OauthView
{
    [Inject] public AppState State { get; set; } = null!;
    
    [Inject] public NavigationManager Nav { get; set; } = null!;

    [Inject] public IHttpClientFactory ClientFactory { get; set; } = null!;

    protected override async Task OnInitializedAsync()
    {
        Uri uri = Nav.ToAbsoluteUri(Nav.Uri);

        NameValueCollection queryParams = HttpUtility.ParseQueryString(uri.Query);
        string? code = queryParams["code"];

        if (string.IsNullOrEmpty(code))
        {
            throw new Exception("Invalid auth");
        }

        using HttpClient client = ClientFactory.CreateClient();
        string redirectUri =
            Nav.ToAbsoluteUri(Nav.Uri).GetLeftPart(UriPartial.Authority) +
            "/oauth";
        UserInfoRequest request = new UserInfoRequest(
            code,
            redirectUri);
        using HttpResponseMessage responseMessage = await client.PostAsync(
            "http://localhost:5000/oauth/handle",
            JsonContent.Create(request, OauthViewContext.Default.UserInfoRequest));

        UserInfoResponse? response = await responseMessage.Content.ReadFromJsonAsync(
            OauthViewContext.Default.UserInfoResponse);
        
        State.Email = response?.Email;
        State.DisplayName = response?.DisplayName;
        State.JobTitle = response?.JobTitle;

        Nav.NavigateTo("/home");
    }

    public record UserInfoRequest(
        string Code,
        string RedirectUri);

    public record UserInfoResponse(
        string? Email,
        string? DisplayName,
        string? JobTitle);
}

[JsonSerializable(typeof(OauthView.UserInfoRequest))]
[JsonSerializable(typeof(OauthView.UserInfoResponse))]
public partial class OauthViewContext : JsonSerializerContext
{
}
