using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using oauth_sample.Client.Services;

namespace oauth_sample.Client.Views;

public partial class HomeView
{
    [Inject] public AppState AppState { get; set; } = null!;
    
    [Inject] public IHttpClientFactory ClientFactory { get; set; } = null!;
    
    [Inject] public NavigationManager Nav { get; set; } = null!;

    string Name
    {
        get;
        set;
    }
    
    string Email
    {
        get;
        set;
    }
    
    string JobTitle
    {
        get;
        set;
    }

    public HomeView()
    {
        Name = "";
        Email = "";
        JobTitle = "";
    }

    protected override void OnInitialized()
    {
        Name = AppState.DisplayName ?? "N/A";
        Email = AppState.Email ?? "N/A";
        JobTitle = AppState.JobTitle ?? "N/A";
    }

    async Task Logout()
    {
        using HttpClient client = ClientFactory.CreateClient();
        string redirectUri = Nav.ToAbsoluteUri(Nav.Uri).GetLeftPart(UriPartial.Authority);
        string oauthUri = await client.GetStringAsync(
            "http://localhost:5000/oauth/logout" +
            $"?redirect={redirectUri}");
        Nav.NavigateTo(oauthUri);
    }
}
