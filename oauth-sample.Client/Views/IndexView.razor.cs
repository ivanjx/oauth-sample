using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace oauth_sample.Client.Views;

public partial class IndexView
{
    [Inject] public IHttpClientFactory ClientFactory { get; set; } = null!;
    
    [Inject] public NavigationManager Nav { get; set; } = null!;
    
    async Task Login()
    {
        using HttpClient client = ClientFactory.CreateClient();
        string redirectUri =
            Nav.ToAbsoluteUri(Nav.Uri).GetLeftPart(UriPartial.Authority) +
            "/oauth";
        string oauthUri = await client.GetStringAsync(
            "http://localhost:5000/oauth/link" +
            $"?redirect={redirectUri}");
        Nav.NavigateTo(oauthUri);
    }
}
