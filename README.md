## Azure App Registration
- Go to https://portal.azure.com.
- Search for `App registrations`.
- Click `New registration`.
- Input app name, select supported account types accordingly.
- For `Redirect URI`, select `Web` and input your client web's oauth handler (in this project I use `http://localhost:5004/oauth`, see Views/OauthView.razor.cs).
- Click the app name on the app registrations list. Save the `Application (client) ID` and `Directory (tenant) ID`.
- Go to `Certificates & secrets`. Click `New client secret`. Select appropriate expires and remember to RENEW it accordingly.
- Save the `Value` part. It is the client secret for the oauth process.

## Azure User Registration
- Go to https://portal.azure.com.
- Search for `Users`.
- Click `New user`. Either choose to invite or create a new user.
