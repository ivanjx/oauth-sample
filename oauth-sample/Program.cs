using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

// Initialization.
var builder = WebApplication.CreateSlimBuilder(args);
builder.Services.AddHttpClient();
builder.Services.AddCors();
builder.Services.ConfigureHttpJsonOptions(options =>
{
    options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
});
var app = builder.Build();

// Static configurations.
const string DIRECTORY_ID = "00696584-ed6b-4e88-aeea-702659db2975";
const string APP_ID = "b551ac1b-3c72-4eb5-9322-f1770555e6d6";
const string APP_SECRET = "HXs8Q~Fc~9zrkZDXSBC2honIVQTN-i4ZeKFF2bIU";

string SCOPE = "https://graph.microsoft.com/user.read";

// Route handlers.
app.MapGet(
    "/oauth/link",
    (HttpContext context) =>
        $"https://login.microsoftonline.com/{DIRECTORY_ID}/oauth2/v2.0/authorize?" +
        $"client_id={APP_ID}&" +
        $"redirect_uri={HttpUtility.UrlEncode(context.Request.Query["redirect"])}&" +
        $"response_type=code&" +
        $"response_mode=query&" +
        $"scope={HttpUtility.UrlEncode(SCOPE)}");

app.MapGet(
    "/oauth/logout",
    (HttpContext context) =>
        $"https://login.microsoftonline.com/{DIRECTORY_ID}/oauth2/v2.0/logout?" +
        $"post_logout_redirect_uri={HttpUtility.UrlEncode(context.Request.Query["redirect"])}");

app.MapPost(
    "/oauth/handle",
    async (
        IHttpClientFactory clientFactory,
        UserInfoRequest? request) =>
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Code) ||
            string.IsNullOrEmpty(request.RedirectUri))
        {
            throw new ArgumentException("missing_parameters");
        }

        Console.WriteLine("Getting oauth token");
        using HttpClient client = clientFactory.CreateClient();
        Dictionary<string, string> tokenRequest = new()
        {
            { "client_id", APP_ID },
            { "client_secret", APP_SECRET },
            { "scope", SCOPE },
            { "redirect_uri", request.RedirectUri },
            { "grant_type", "authorization_code" },
            { "code", request.Code }
        };
        using HttpResponseMessage tokenResponseMessage = await client.PostAsync(
            $"https://login.microsoftonline.com/{DIRECTORY_ID}/oauth2/v2.0/token",
            new FormUrlEncodedContent(tokenRequest));
        OauthTokenResponse? tokenResponse = await tokenResponseMessage.Content.ReadFromJsonAsync(
            AppJsonSerializerContext.Default.OauthTokenResponse);

        if (tokenResponse == null ||
            string.IsNullOrEmpty(tokenResponse.AccessToken))
        {
            Exception? innerEx = null;

            if (tokenResponse != null &&
                !string.IsNullOrEmpty(tokenResponse.Error))
            {
                innerEx = new Exception(tokenResponse.Error);
            }
            
            throw new Exception("Invalid oauth token response", innerEx);
        }

        Console.WriteLine("Querying user info");
        using HttpRequestMessage userInfoRequestMessage = new HttpRequestMessage();
        userInfoRequestMessage.RequestUri = new Uri(
            "https://graph.microsoft.com/v1.0/me");
        userInfoRequestMessage.Method = HttpMethod.Get;
        userInfoRequestMessage.Headers.Authorization = new AuthenticationHeaderValue(
            "Bearer",
            tokenResponse.AccessToken);
        using HttpResponseMessage userInfoResponseMessage = await client.SendAsync(
            userInfoRequestMessage);

        OauthUserInfoResponse? oauthUserInfo = await userInfoResponseMessage.Content.ReadFromJsonAsync(
            AppJsonSerializerContext.Default.OauthUserInfoResponse);

        if (oauthUserInfo == null)
        {
            Exception? innerEx = null;

            if (oauthUserInfo != null &&
                !string.IsNullOrEmpty(oauthUserInfo.Error))
            {
                innerEx = new Exception(oauthUserInfo.Error);
            }
            
            throw new Exception("Invalid oauth user info response", innerEx);
        }
        
        UserInfoResponse result = new UserInfoResponse(
            oauthUserInfo.Email,
            oauthUserInfo.DisplayName,
            oauthUserInfo.JobTitle);
        return Results.Json(
            result,
            AppJsonSerializerContext.Default.UserInfoResponse);
    });

// Cors stuff.
app.UseCors(builder => builder
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowCredentials()
    .SetIsOriginAllowed(origin =>
        origin.Contains("localhost"))); // Allow from localhost.

// Done.
app.Run();

// Models.
public record UserInfoRequest(
    string? Code,
    string? RedirectUri);

public record UserInfoResponse(
    string? Email,
    string? DisplayName,
    string? JobTitle);

public record OauthTokenResponse
{
    [JsonPropertyName("error_description")]
    public string? Error
    {
        get;
        set;
    }
    
    [JsonPropertyName("access_token")]
    public string? AccessToken
    {
        get;
        set;
    }
}

public record OauthUserInfoResponse
{
    [JsonPropertyName("error_description")]
    public string? Error
    {
        get;
        set;
    }
    
    [JsonPropertyName("displayName")]
    public string? DisplayName
    {
        get;
        set;
    }

    [JsonPropertyName("userPrincipalName")]
    public string? Email
    {
        get;
        set;
    }

    [JsonPropertyName("jobTitle")]
    public string? JobTitle
    {
        get;
        set;
    }
}

[JsonSerializable(typeof(UserInfoRequest))]
[JsonSerializable(typeof(UserInfoResponse))]
[JsonSerializable(typeof(OauthTokenResponse))]
[JsonSerializable(typeof(OauthUserInfoResponse))]
internal partial class AppJsonSerializerContext : JsonSerializerContext
{
}
